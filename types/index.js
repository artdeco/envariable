export {}

/* typal types/api.xml namespace */
/**
 * @typedef {_envariable.envariable} envariable Read `.env` Into Environment Variables (in `process.env`).
 * @typedef {(config: !_envariable.Config) => void} _envariable.envariable Read `.env` Into Environment Variables (in `process.env`).
 */

/**
 * @typedef {import('..').Config} _envariable.Config
 */