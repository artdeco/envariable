/**
 * @fileoverview
 * @externs
 */

/* typal types/index.xml externs */
/** @const */
var _envariable = {}
/**
 * Options for the program.
 * @record
 */
_envariable.Config
/**
 * Do not print the names of read env variables. Default `false`.
 * @type {boolean|undefined}
 */
_envariable.Config.prototype.silent
/**
 * The location where to look up the `.env` file. Default `~`.
 * @type {string|undefined}
 */
_envariable.Config.prototype.location
/**
 * The name of the `.env` file. Default `.env`.
 * @type {string|undefined}
 */
_envariable.Config.prototype.name

/* typal types/api.xml externs */
/**
 * Read `.env` Into Environment Variables (in `process.env`).
 * @typedef {function(!_envariable.Config): void}
 */
_envariable.envariable
