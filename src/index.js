import { readFileSync } from 'fs'
import { join } from 'path'
import { EOL } from 'os'

/**
 * @type {_envariable.envariable}
 */
export default function dotenv(config = {}) {
  const {
    silent = false,
    location = process.cwd(),
    name = '.env',
  } = config
  try {
    const j = join(location, name)
    const f = readFileSync(j, 'utf8')
    const ff = f.split(/\r?\n/)
    ff.forEach(env => {
      const [e, ...rest] = env.split('=')
      !silent && process.stdout.write(`[+] ${e} `)
      process.env[e] = rest.join('=')
    })
    !silent && process.stdout.write(EOL)
  } catch (err) {
    console.log(err)
  }
}

/**
 * @suppress {nonStandardJsDocs}
 * @typedef {import('../types').envariable} _envariable.envariable
 */
