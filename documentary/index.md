<div align="center">

# envariable

%NPM: envariable%
<pipeline-badge/>

</div>

`envariable` is Read .env Into Environment Variables.

```sh
yarn add envariable
npm i envariable
```

## Table Of Contents

%TOC%

%~%