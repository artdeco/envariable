## API

The package is available by importing its default function:

```js
import envariable from 'envariable'
```

%~%

<typedef method="envariable">types/api.xml</typedef>

<typedef>types/index.xml</typedef>

_For example, consider the usage for the following `.env` file_:

%EXAMPLE: example/.env%

%EXAMPLE: example, ../src => envariable%
%FORK example%

%~%