<div align="center">

# envariable

[![npm version](https://badge.fury.io/js/envariable.svg)](https://www.npmjs.com/package/envariable)
<a href="https://gitlab.com/artdeco/envariable/-/commits/master">
  <img src="https://gitlab.com/artdeco/envariable/badges/master/pipeline.svg" alt="Pipeline Badge">
</a>

</div>

`envariable` is Read .env Into Environment Variables.

```sh
yarn add envariable
npm i envariable
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`envariable(config: !Config): void`](#envariableconfig-config-void)
  * [`Config`](#type-config)
- [Copyright & License](#copyright--license)

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

## API

The package is available by importing its default function:

```js
import envariable from 'envariable'
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>

## <code><ins>envariable</ins>(</code><sub><br/>&nbsp;&nbsp;`config: !Config,`<br/></sub><code>): <i>void</i></code>
Read `.env` Into Environment Variables (in `process.env`).

 - <kbd><strong>config*</strong></kbd> <em><code><a href="#type-config" title="Options for the program.">!Config</a></code></em>: Options for the program.

__<a name="type-config">`Config`</a>__: Options for the program.


|   Name   |       Type       |                  Description                   | Default |
| -------- | ---------------- | ---------------------------------------------- | ------- |
| silent   | <em>boolean</em> | Do not print the names of read env variables.  | `false` |
| location | <em>string</em>  | The location where to look up the `.env` file. | `~`     |
| name     | <em>string</em>  | The name of the `.env` file.                   | `.env`  |

_For example, consider the usage for the following `.env` file_:

```
HELLO=WORLD
```

```js
import envariable from 'envariable'

envariable({
  location: 'example',
})
console.log(process.env.HELLO)
```
```
[+] HELLO 
WORLD
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/2.svg?sanitize=true">
</a></div>



## Copyright & License

GNU Affero General Public License v3.0

<table>
  <tr>
    <th>
      <a href="https://www.artd.eco">
        <img width="100" src="https://gitlab.com/uploads/-/system/group/avatar/7454762/artdeco.png"
          alt="Art Deco">
      </a>
    </th>
    <th>© <a href="https://www.artd.eco">Art Deco™</a>   2020</th>
    <th><a href="LICENSE"><img src=".documentary/agpl-3.0.svg" alt="AGPL-3.0"></a></th>
  </tr>
</table>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>