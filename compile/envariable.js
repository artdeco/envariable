#!/usr/bin/env node
'use strict';
const fs = require('fs');
const path = require('path');
const os = require('os');             const d=fs.readFileSync;const e=path.join;const f=os.EOL;/*
 envariable: Read .env Into Environment Variables.

 Copyright (C) 2020  Art Deco Code Limited

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
module.exports={$envariable:function(g={}){const {silent:b=!1,location:h=process.cwd(),name:k=".env"}=g;try{const a=e(h,k);d(a,"utf8").split(/\r?\n/).forEach(l=>{const [c,...m]=l.split("=");!b&&process.stdout.write(`[+] ${c} `);process.env[c]=m.join("=")});!b&&process.stdout.write(f)}catch(a){console.log(a)}}};

//# sourceMappingURL=envariable.js.map