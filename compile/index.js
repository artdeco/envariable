const { $envariable } = require('./envariable')

/**
 * Read `.env` Into Environment Variables (in `process.env`).
 * @param {!_envariable.Config} config Options for the program.
 * @param {boolean} [config.silent=false] Do not print the names of read env variables. Default `false`.
 * @param {string} [config.location="~"] The location where to look up the `.env` file. Default `~`.
 * @param {string} [config.name=".env"] The name of the `.env` file. Default `.env`.
 */
function envariable(config) {
  return $envariable(config)
}

module.exports = envariable

/* typal types/index.xml namespace */
/**
 * @typedef {_envariable.Config} Config `＠record` Options for the program.
 * @typedef {Object} _envariable.Config `＠record` Options for the program.
 * @prop {boolean} [silent=false] Do not print the names of read env variables. Default `false`.
 * @prop {string} [location="~"] The location where to look up the `.env` file. Default `~`.
 * @prop {string} [name=".env"] The name of the `.env` file. Default `.env`.
 */
