const { $envariable } = require('./envariable')

/**
 * @methodType {_envariable.envariable}
 */
function envariable(config) {
  return $envariable(config)
}

module.exports = envariable

/* typal types/index.xml namespace */
