## 13 April 2020

### [1.0.0](https://gitlab.com/artdeco/envariable/compare/v0.0.0-pre...v1.0.0)

- [package] Publish `v1` of the package.

### 0.0.0-pre

- Create `envariable` with _[`NodeTools`](https://art-deco.github.io/nodetools)_.